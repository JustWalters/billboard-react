import React, { Component } from 'react';
import DatePicker from 'react-date-picker';

class ChartForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			charts: [],
			chart: 'hot-100',
			date: this.props.date
		};
	}

	componentDidMount() {
		this.getCharts();
	}

	getCharts() {
		let url = '/api/allCharts';
		this.props.onLoadingChange(true);
		fetch(url, {
			headers: {
				accept: 'application/json',
			}
		})
			.then(res => res.json())
			.then(charts => charts.map(chart => ({ label: chart.name, value: chart.urlName })))
			.then(charts => this.setState({ charts }))
			.catch(this.handleError)
			.finally(() => this.props.onLoadingChange(false));
	}

	optionsFromCharts() {
		return (
			this.state.charts.map(chart => (
				<option value={chart.value} key={chart.value}>{chart.label}</option>
			))
		);
	}

	handleError(err) {
		console.error(err);
	}

	onDateChange = date => this.setState({ date });

	handleChange = event => this.setState({ chart: event.target.value });

	handleSubmit = event => {
		event.preventDefault();
		this.props.onSubmit({
			chart: this.state.chart,
			date: this.state.date
		});
	};

	render() {
		let options = this.state.charts && this.state.charts.length
			? this.optionsFromCharts()
			: <option value="hot-100">Hot 100</option>;

		return (
			<form onSubmit={this.handleSubmit}>
				<select value={this.state.chart} onChange={this.handleChange}>
					{options}
				</select>
				<DatePicker
					value={this.state.date}
					onChange={this.onDateChange}
					maxDate={new Date()}
					required={true}
				/>
				<input type="submit" value="Search" />
			</form>
		);
	}
}

ChartForm.defaultProps = {
	date: new Date(),
	onSubmit: () => { },
	onLoadingChange: () => { }
};

export default ChartForm;
