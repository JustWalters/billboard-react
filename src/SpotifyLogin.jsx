import React from "react";

const stateKey = "spotify_auth_state";
const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
const REDIRECT_URI = process.env.REACT_APP_REDIRECT_URI;

const generateRandomString = length => {
	let text = "";
	const possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	while (text.length <= length) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return text;
};

const SpotifyLogin = class extends React.Component {
	componentDidMount() {
		localStorage.removeItem(stateKey);
		this.checkHash()
	}

	checkHash = () => {
		const hash = window.location.hash.replace(/#/g, '');
		const all = hash.split('&');
		const args = {};
		if (!hash) {
			return
		}
		all.forEach(function(keyvalue) {
			const index = keyvalue.indexOf('=');
			const key = keyvalue.substring(0, index);
			const val = keyvalue.substring(index + 1);
			args[key] = val;
		});


		if (typeof (args['access_token']) !== 'undefined') {
			this.props.onAccessToken(args['access_token'])
		}
	}

	handleClick = () => {
		const client_id = CLIENT_ID;
		const redirect_uri = REDIRECT_URI;
		const scope = "user-read-private user-read-email playlist-modify-public playlist-modify-private";
		const state = generateRandomString(16);

		localStorage.setItem(stateKey, state);

		const url = `https://accounts.spotify.com/authorize
		?response_type=token
		&client_id=${encodeURIComponent(client_id)}
		&scope=${encodeURIComponent(scope)}
		&redirect_uri=${encodeURIComponent(redirect_uri)}
		&state=${encodeURIComponent(state)}`;

		window.location = url;
	};

	render() {
		return <button onClick={this.handleClick}>Log in</button>;
	}
};

export default SpotifyLogin;
