import React from 'react';
import ReactDOM from 'react-dom';
import { StyleSheetTestUtils } from 'aphrodite';
import App from './App';

beforeAll(() => {
	global.fetch = url => Promise.resolve({ json() { return {}; } });
	Promise.prototype.finally = Promise.prototype.then;
	StyleSheetTestUtils.suppressStyleInjection();
});

afterAll(() => {
  StyleSheetTestUtils.clearBufferAndResumeStyleInjection();
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
	ReactDOM.unmountComponentAtNode(div);
});
