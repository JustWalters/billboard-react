import React, { Component } from 'react';
import LoadingIndicator from 'react-loading-indicator';
import ChartForm from './ChartForm';
import ChartList from './ChartList';
import Spotify from './Spotify';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			chartName: '',
			chart: {},
			songIndexesNotFound: []
		};
	}

	onSubmit = ({ chart: chartName, date }) => {
		let url = `/api/getChart?chart=${chartName}&date=${date.toJSON()}`;
		this.setState({
			isLoading: true
		});
		fetch(url, {
			headers: {
				accept: 'application/json'
			}
		})
			.then(response => response.json())
			.then(chart => this.setState({ chart, chartName }))
			.catch(this.handleError)
			.finally(() => this.setState({ isLoading: false }));
	};

	onLoadingChange = isLoading => this.setState({ isLoading });

	handleSongsNotFound = songIndexes => {
		this.setState({ songIndexesNotFound: songIndexes })
	}

	handleError = (err) => {
		console.error(err);
	};

	render() {
		let songs = (this.state.chart && this.state.chart.songs) || []
		return (
			<div className="App">
				{this.state.isLoading && <LoadingIndicator />}
				<ChartForm onSubmit={this.onSubmit} onLoadingChange={this.onLoadingChange} />
				<Spotify chartName={this.state.chartName} chart={this.state.chart} setIsLoading={this.onLoadingChange} onSongsNotFound={this.handleSongsNotFound} />
				<ChartList songs={songs} songIndexesNotFound={this.state.songIndexesNotFound} />
			</div>
		);
	}
}

export default App;
