import React, { Component } from 'react';
import './ChartList.css';

class ChartList extends Component {
	defaultArt = 'https://assets.billboard.com/assets/1553106965/images/charts/bb-placeholder-new.jpg'

	tableBody() {
		const { songs, songIndexesNotFound } = this.props
		return songs.map((song, index) => <tr key={song.rank} className={songIndexesNotFound.includes(index) ? 'not-found' : ''}>
			<td>{song.rank}</td>
			<td>
				<div
					className="chart-list-img"
					style={{ backgroundImage: `url(${(song.cover || this.defaultArt)})` }}
				></div>
			</td>
			<td>{song.title}</td>
			<td>{song.artist}</td>
		</tr>);
	}

	render() {
		return this.props.songs.length ? (
			<table className="chart-list-table">
				<thead>
					<tr>
						<th>Position</th>
						<th>Art</th>
						<th>Title</th>
						<th>Artist</th>
					</tr>
				</thead>
				<tbody>
					{this.tableBody()}
				</tbody>
			</table>
		) : <h5>No data</h5>;
	}
}

export default ChartList;
