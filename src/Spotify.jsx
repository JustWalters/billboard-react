import React from "react";
import SpotifyApi from "spotify-web-api-js";
import Login from "./SpotifyLogin";

class Spotify extends React.Component {
	constructor(props) {
		super(props)

		this.API = new SpotifyApi()
		this.state = {
			loggedIn: false
		}
	}

	handleAccessToken = (accessToken) => {
		this.API.setAccessToken(accessToken)
		this.setState({ loggedIn: true })
	}

	createPlaylist = async () => {
		const { chartName, chart, setIsLoading } = this.props
		setIsLoading(true)
		try {
			const user = await this.API.getMe()
			const { id } = user || {}

			const playlist = await this.API.createPlaylist(id, { name: `${chartName} for ${new Date(chart.week).toDateString()}`, public: false })
			this.populatePlaylist(playlist.id)
		} finally {
			setIsLoading(false)
		}
	}

	populatePlaylist = async playlistId => {
		const { setIsLoading, chart, onSongsNotFound } = this.props
		setIsLoading(true)
		try {
			const searches = chart.songs.map(async ({ title, artist }) => {
				const results = await this.API.searchTracks(`"${title}" ${artist}`, { limit: 1 })
				const { tracks: { items: [{ uri } = {}] = [] } = {} } = results
				return uri
			})
			const uris = await Promise.all(searches)

			const validUris = []
			const notFoundIndexes = []
			uris.forEach((uri, index) => {
				if (uri) validUris.push(uri)
				else notFoundIndexes.push(index)
			})

			notFoundIndexes.length && onSongsNotFound(notFoundIndexes)
			const response = await this.API.addTracksToPlaylist(playlistId, validUris)
			console.log({ response })
			alert('Check it out!')
		} finally {
			setIsLoading(false)
		}
	}

	render() {
		const { loggedIn } = this.state
		const { chart } = this.props
		const readyToCreate = loggedIn && chart.songs && chart.week
		return <React.Fragment>
			{loggedIn || <Login onAccessToken={this.handleAccessToken} />}
			{readyToCreate && <button onClick={this.createPlaylist}>Create playlist</button>}
		</React.Fragment>;
	}
};


export default Spotify;
